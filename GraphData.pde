class GraphData {
  ArrayList<Float> data = new ArrayList<Float>();

  GraphData () {
  }

  void addData(float value) {
    data.add(value);
  }

  float maxValue() {
    float max = Integer.MIN_VALUE;
    for (int i = 0; i<data.size(); i++) {
      if (data.get(i) > max) {
        max = data.get(i);
      }
    }
    return max;
  }
}
