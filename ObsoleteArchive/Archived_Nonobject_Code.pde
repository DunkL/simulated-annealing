/*
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE

int [] RandomGenerateColorsOfOneRing(int NumberOfColors_BetweenOneandNine) {
  int [] RingColorArray = new int[numberOfRingSections];
  for (int i = 0; i < numberOfRingSections; i++) {
    RingColorArray[i] = (int) random(0, NumberOfColors_BetweenOneandNine);
  }
  return RingColorArray;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Done
int [] [] GenerateAllRings(int TotalNumberOfRings, int NumberOfColors_BetweenOneandNine) {
  int [] [] ArrayOfAllRings = new int[TotalNumberOfRings][numberOfRingSections];
  for (int i = 0; i < TotalNumberOfRings; i++) {
    ArrayOfAllRings[i] = RandomGenerateColorsOfOneRing(NumberOfColors_BetweenOneandNine);
  }
  return ArrayOfAllRings;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
void DrawRingSectionAtOrigin (float x1_DistFromOrigin, float y1_DistFromOrigin, float x2_DistFromOrigin, float y2_DistFromOrigin, color RingSectionColor) {
  noStroke();
  fill(RingSectionColor);
  triangle(x_Origin, y_Origin, x_Origin + x1_DistFromOrigin, y_Origin + y1_DistFromOrigin, x_Origin + x2_DistFromOrigin, y_Origin + y2_DistFromOrigin);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
void DrawOneRing(float Radius, int [] RingColorArray) {
  float [] x_PositionFromOrigin = {};
  float [] y_PositionFromOrigin = {};
  //Obtain position data
  for (int i = 0; i < numberOfRingSections + 1; i++) {
    x_PositionFromOrigin = append(x_PositionFromOrigin, Radius * CosineTwelvePartCircleRadians[i]);
    y_PositionFromOrigin = append(y_PositionFromOrigin, Radius * SineTwelvePartCircleRadians[i]);
  }
  //Draw rings
  for (int i = 0; i < numberOfRingSections; i++) {
    DrawRingSectionAtOrigin(x_PositionFromOrigin[i], y_PositionFromOrigin[i], x_PositionFromOrigin[i+1], y_PositionFromOrigin[i+1], Color[RingColorArray[i]]);
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
void DrawAllRings(int TotalNumberOfRings, int [] [] ArrayOfAllRings) {
  float RingSeparationDistance = radius_Diff/TotalNumberOfRings;
  for (int i = 0; i < TotalNumberOfRings; i++) {
    DrawOneRing(radius_Max - i * RingSeparationDistance, ArrayOfAllRings[i]);
  }
  //Draw a black central ring
  DrawOneRing(radius_Min, CenterBlackCircle);
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
int ScoreOneRingSection(int TotalNumberOfRings, int RingID, int SectionID, int [] [] ArrayOfAllRings) {
  int SelectedSectionColor = ArrayOfAllRings[RingID][SectionID];
  int SelectedSectionScore = 0;

  //Boolean checks
  boolean Add_toRingID = (RingID < TotalNumberOfRings-1);
  boolean Sub_toRingID = (RingID > 0);
  boolean Add_toSectionID = (SectionID < numberOfRingSections - 1);
  boolean Sub_toSectionID = (SectionID > 0);
  boolean Zero_SectionID = (SectionID == 0);
  boolean Max_SectionID = (SectionID == numberOfRingSections-1);
  //boolean Exceed_Zero_Section = (SectionID-1 < 0);
  //boolean Exceed_Max_Section = (SectionID+1 > numberOfRingSections);

  //Regular adjacent cases
  if (Add_toRingID && (SelectedSectionColor == ArrayOfAllRings[RingID+1][SectionID])) { 
    SelectedSectionScore+=2;
  }
  if (Sub_toRingID && (SelectedSectionColor == ArrayOfAllRings[RingID-1][SectionID])) { 
    SelectedSectionScore+=2;
  }
  //if (Add_toSectionID && (SelectedSectionColor == ArrayOfAllRings[RingID][SectionID+1])) { SelectedSectionScore+=2; }
  //if (Sub_toSectionID && (SelectedSectionColor == ArrayOfAllRings[RingID][SectionID-1])) { SelectedSectionScore+=2; }

  // Edge case adjacent cases
  //if (Zero_SectionID && (ArrayOfAllRings[RingID][0] == ArrayOfAllRings[RingID][numberOfRingSections-1])) { SelectedSectionScore+=2; }
  //if (Max_SectionID && (ArrayOfAllRings[RingID][0] == ArrayOfAllRings[RingID][0])) { SelectedSectionScore+=2; }

  //Regular diagonal cases
  if (Add_toRingID && Add_toSectionID && (SelectedSectionColor == ArrayOfAllRings[RingID+1][SectionID+1])) { 
    SelectedSectionScore++;
  }
  if (Add_toRingID && Sub_toSectionID && (SelectedSectionColor == ArrayOfAllRings[RingID+1][SectionID-1])) { 
    SelectedSectionScore++;
  }
  if (Sub_toRingID && Add_toSectionID && (SelectedSectionColor == ArrayOfAllRings[RingID-1][SectionID+1])) { 
    SelectedSectionScore++;
  }
  if (Sub_toRingID && Sub_toSectionID && (SelectedSectionColor == ArrayOfAllRings[RingID-1][SectionID-1])) { 
    SelectedSectionScore++;
  }

  //Edge case diagonal cases
  if (Add_toRingID && Zero_SectionID && (SelectedSectionColor == ArrayOfAllRings[RingID+1][numberOfRingSections-1])) { 
    SelectedSectionScore++;
  }
  if (Sub_toRingID && Zero_SectionID && (SelectedSectionColor == ArrayOfAllRings[RingID-1][numberOfRingSections-1])) { 
    SelectedSectionScore++;
  }
  if (Add_toRingID && Max_SectionID && (SelectedSectionColor == ArrayOfAllRings[RingID+1][0])) { 
    SelectedSectionScore++;
  }
  if (Sub_toRingID && Max_SectionID && (SelectedSectionColor == ArrayOfAllRings[RingID-1][0])) { 
    SelectedSectionScore++;
  }

  return SelectedSectionScore;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
int ScoreAllRings(int TotalNumberOfRings, int [] [] ArrayOfAllRings) { //Note: Cost is proportional to negative Score.
  int TotalScore = 0;
  for (int RingID = 0; RingID < TotalNumberOfRings; RingID++) {
    for (int SectionID = 0; SectionID < numberOfRingSections; SectionID++) {
      TotalScore = TotalScore + ScoreOneRingSection(TotalNumberOfRings, RingID, SectionID, ArrayOfAllRings);
    }
  }
  return TotalScore;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
int [] [] SwapTwoRings(int RingID_1, int RingID_2, int [] [] ArrayOfAllRings) {
  int [] tempStorage = ArrayOfAllRings[RingID_1];
  ArrayOfAllRings[RingID_1] = ArrayOfAllRings[RingID_2];
  ArrayOfAllRings[RingID_2] = tempStorage;
  return ArrayOfAllRings;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//DONE
int [] [] RotateRing(int RingID, int [] [] ArrayOfAllRings) {
  int tempStorage = ArrayOfAllRings[RingID][0];
  for (int i = 0; i < numberOfRingSections - 1; i++) {
    ArrayOfAllRings[RingID][i] = ArrayOfAllRings[RingID][i+1];
  }
  ArrayOfAllRings[RingID][numberOfRingSections - 1] = tempStorage;
  return ArrayOfAllRings;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

int [] [] Perturb(int TotalNumberOfRings, int [] [] ArrayOfAllRings) {
  int rand_Control = (int) random(0, 2);
  if (rand_Control == 0) {
    int rand_RingID_1 = (int) random(0, TotalNumberOfRings);
    int rand_RingID_2 = (int) random(0, TotalNumberOfRings);
    ArrayOfAllRings = SwapTwoRings(rand_RingID_1, rand_RingID_2, ArrayOfAllRings);
  } else {
    int rand_NumberofRotations = (int) random(0, 11);
    int rand_RingID = (int) random(0, TotalNumberOfRings);
    for (int i = 0; i < rand_NumberofRotations; i++) {
      ArrayOfAllRings = RotateRing(rand_RingID, ArrayOfAllRings);
    }
  }
  return ArrayOfAllRings;
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void arrayCopy2DIntArray(int ArrayLength, int [] [] SrcArray, int [] [] DstArray){
  //This function took 2 hours of debugging to create.
  for (int i = 0; i < ArrayLength; i++){
    arrayCopy(SrcArray[i], DstArray[i]);
  }
}
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
*/
