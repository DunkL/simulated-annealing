class Rectangle {
  int x, y, w, h;

  Rectangle(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  boolean pointInside(int pointX, int pointY) {
    return pointX > x && pointX < x + w && pointY > y && pointY < y + h;
  }
}