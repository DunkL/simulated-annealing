class Slider {
  int centerX;
  int centerY;
  
  private int barLength;
  private int barThickness;
  private boolean horizontalOrientation = true;
  color barColor = color(0, 0, 0);
  
  int indicatorSize = 20;                        // Indicator is the triangle
  color indicatorColor = color(240, 240, 240);
  
  private float min = 0;
  private float max = 1;
  
  float value;
  
  private boolean dragging = false;
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max) throws IllegalArgumentException{
    centerX = x;
    centerY = y;
    this.barLength = barLength;
    this.barThickness = barThickness;
    
    if(barLength < 0 || barThickness < 0) throw new IllegalArgumentException("Dimensions must be positive");
    if(min > max) throw new IllegalArgumentException("Min cannot be greater than max");
    
    this.min = min;
    this.max = max;
  }
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max, float initialValue) throws IllegalArgumentException {
    this(x, y, barLength, barThickness, min, max);
    value = initialValue;
  }
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max, boolean horizontalOrientation) throws IllegalArgumentException {
    this(x, y, barLength, barThickness, min, max);
    this.horizontalOrientation = horizontalOrientation;
  }
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max, float initialValue, boolean horizontalOrientation) throws IllegalArgumentException {
    this(x, y, barLength, barThickness, min, max, initialValue);
    this.horizontalOrientation = horizontalOrientation;
  }
  
  private float range() {
    return max - min;
  }
  
  private float centerValue() {
    return (max + min) * 0.5;
  }
  
  void mouseDown(int xMouse, int yMouse) {
    if (sliderArea().pointInside(xMouse, yMouse) || dragging) {
      value = mouseSliderValue(xMouse, yMouse);
      
      if (value > max) value = max;
      else if (value < min) value = min;
      
      dragging = true;
    }
  }
  
  void mouseUp() {
    dragging = false;
  }
  
  Rectangle sliderArea() {
    if (horizontalOrientation) {
      return new Rectangle(int(centerX - barLength * 0.5), int(centerY - barThickness * 0.5), barLength, barThickness);
    } else {
      return new Rectangle(int(centerX - barThickness * 0.5), int(centerY - barLength * 0.5), barThickness, barLength);
    }
  }
  
  private float mouseSliderValue(int xMouse, int yMouse) {
    return centerValue() + mouseSliderFraction(xMouse, yMouse) * range() * 0.5;
  }
  
  private float mouseSliderFraction(int xMouse, int yMouse) {  // between -1 to 1
    if (horizontalOrientation) {
      return float(xMouse - centerX) / barLength * 2;
    } else {
      return float(yMouse - centerY) / barLength * 2;
    }
  }
  
  
  
  void drawSlider() {
    drawBar();
    drawIndicator();
  }
  
  private void drawBar() {
    fill(barColor);
    Rectangle bar = sliderArea();
    rect(bar.x, bar.y, bar.w, bar.h);
  }
  
  private void drawIndicator() {
    fill(indicatorColor);
    float startingAngle = PI * 0.5;
    float indicatorOffset = (value - centerValue()) / range() * barLength;
    
    if (horizontalOrientation) {
      triangle(
        centerX + indicatorOffset + indicatorSize * cos(startingAngle),
        centerY + indicatorSize * sin(startingAngle), 
        centerX + indicatorOffset + indicatorSize * cos(startingAngle + PI * 2/3), 
        centerY + indicatorSize * sin(startingAngle + PI * 2/3), 
        centerX + indicatorOffset + indicatorSize * cos(startingAngle + PI * 4/3), 
        centerY + indicatorSize * sin(startingAngle + PI * 4/3)
      );
    } else {
      startingAngle = 0;
      triangle(
        centerX + indicatorSize * cos(startingAngle),
        centerY + indicatorOffset + indicatorSize * sin(startingAngle), 
        centerX + indicatorSize * cos(startingAngle + PI * 2/3), 
        centerY + indicatorOffset + indicatorSize * sin(startingAngle + PI * 2/3), 
        centerX + indicatorSize * cos(startingAngle + PI * 4/3), 
        centerY + indicatorOffset + indicatorSize * sin(startingAngle + PI * 4/3)
      );
    }
  }
}