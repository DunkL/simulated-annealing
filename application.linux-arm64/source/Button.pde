class Button {
  String text;
  color textColor;
  color backgroundColor;

  Rectangle dimensions;

  Button(String text, Rectangle dimensions) {
    this.text = text;
    this.dimensions = dimensions;
  }

  Button(String text, Rectangle dimensions, color backgroundColor) {
    this(text, dimensions);
    this.backgroundColor = backgroundColor;
  }

  Button(String text, Rectangle dimensions, color backgroundColor, color textColor) {
    this(text, dimensions, backgroundColor);
    this.textColor = textColor;
  }

  boolean mouseOver(int xMouse, int yMouse) {
    return dimensions.pointInside(xMouse, yMouse);
  }

  void drawButton() {
    fill(backgroundColor);
    rect(dimensions.x, dimensions.y, dimensions.w, dimensions.h);

    fill(textColor);
    textAlign(CENTER, CENTER);
    text(text, dimensions.x + dimensions.w * 0.5, dimensions.y + dimensions.h * 0.5);
  }
}
