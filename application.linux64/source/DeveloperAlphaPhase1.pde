//Default Game Settings (edittable)
int NumberOfColors_BetweenOneandNine = 5; //value between 0 and 9
int numberOfRingSections = 10; // value greater than or equal to 3
int TotalNumberOfRings = 10; // value greater than or equal to 1

//Simulated Annealing Settings (editable)
int NumberOfExperiments = 10000; // used in setting an initial temperature any positive value
float DesiredAcceptanceRate_BetweenZeroAndOne = 0.9; // any float between 0 and 1
float Temperature_Min = 0.8; // any float greater than 0
float CoolingRate = 0.99; // any float, recommend to use a value between 0 and 1

//Plan: Have these all editable within the program


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Static global variables

//Available ring colors
color [] Color = {color(255, 0, 0), color(0, 255, 0), color(0, 0, 255), color(255, 255, 0), color(0, 255, 255), color(255, 0, 255), color(255, 255, 255), color(0, 0, 0), color(125, 125, 125)};
static final float x_Origin = 400; //Center of rings x
static final float y_Origin = 350; //Center of rings y
static final int Radius_Max = 350; //Radius of rings

TrigonmetryLookUpTable TrigTable; //Used in drawing ring sections
SimulatedAnnealingState SA_Case;
Graph graph;
GraphData graphData;



///////////////////////////////////////////////////////////////////////////////////////////////////////////


void setup() {  
  size(1600, 1000);
  GameSetup();
}

void draw() { 
  //Setup the next frame to be drawn
  background(50);
  graph.drawGraph(); //clear background


  //State drawing machine (starts in Menu.SIMULATEDANNEALING temp for debugging)
  switch(CurrentMenuState) {

  case Menu.HOME:
    drawButtons(SA_Before_During);
    SA_Case.CurrentState.Draw(Radius_Max, TrigTable); //draw current state as placeholder/final, may be drawn over later
    Draw_SA_Scores();
    Draw_SA_Temperature();
    break;


  case Menu.SIMULATEDANNEALING:
    drawButtons(SA_Before_During);
    SA_Case.CurrentState.Draw(Radius_Max, TrigTable); //draw current state as placeholder/final, may be drawn over later
    if ( /* NOT */ ! SA_Case.Temperature_BelowMin && AutoPlaySA_Flag) {
      SA_Case.OneIterationOfSimulatedAnnealing();
      graphData.addData(SA_Case.CurrentScore - SA_Case.InitialScore * 0.8);
    } else if (SA_Case.Temperature_BelowMin) {
      CurrentMenuState = Menu.DONE_SIMULATEDANNEALING;
    }
    Draw_SA_Scores();
    Draw_SA_Temperature();
    
    drawSliders(SA_Sliders);
    break;


  case Menu.DONE_SIMULATEDANNEALING:
    drawButtons(SACompletedButtons);
    if (DisplayInitSol_Flag) {
      SA_Case.InitialState.Draw(Radius_Max, TrigTable);
    } else {
      SA_Case.BestState.Draw(Radius_Max, TrigTable);
    }
    Draw_SA_Scores();
    Draw_SA_Temperature();
    break;
  }
}





////////////////////////////////////////////
