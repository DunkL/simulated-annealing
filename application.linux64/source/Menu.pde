//Menu states
int CurrentMenuState = Menu.SIMULATEDANNEALING;//tempfor debugging //HOME; //Initialize menu state to home

static abstract class Menu {
  static final int HOME = 0;
  static final int SIMULATEDANNEALING = 1;
  static final int DONE_SIMULATEDANNEALING = 2;
}

//Button draw menu screen from button array
void drawButtons(Button [] currentButtons) {
  for (int i = 0; i < currentButtons.length; i++) {
    currentButtons[i].drawButton();
  }
}

void drawSliders(Slider [] currentSliders) {
  for (int i = 0; i < currentSliders.length; i++) {
    currentSliders[i].drawSlider();
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Button arrays

//Global Buttons
Button ResetSA = new Button("Reset", new Rectangle(800, 320, 250, 50), color(200), color(50));
Button NewGame = new Button("Create New Game", new Rectangle(800, 380, 250, 50), color(200), color(50));
//Button for return home


//Home menu buttons
//CurrentMenuState = Menu.HOME
//Button for how to play
//Button for play
//Button for challenge board
//Button for credits??



//How to play
//Next screen
//return Home
//game start
//rotation
//swap
//scoring


//Play settings menu
//Number of rings
//Number of sections
//Game mode selection (solo, computer simulation)


//Button while SA is active
//CurrentMenuState = Menu.SIMULATEDANNEALING
Button PausePlaySA = new Button("Pause/Play", new Rectangle(800, 200, 250, 50), color(200), color(50));
Button IterateSA = new Button("Do 1 Iteration", new Rectangle(800, 260, 250, 50), color(200), color(50));
Button [] SA_Before_During = {
  PausePlaySA, IterateSA, ResetSA, NewGame
};

boolean AutoPlaySA_Flag = false;
//Sliders SA settings (set init temp/desired acceptance, min temp, cooling rate (0<1)
//Apply new settings
//Button for return home


//Buttons when SA is done
//CurrentMenuState = Menu.DONE_SIMULATEDANNEALING
Button DisplayBestSolution = new Button("Show Best", new Rectangle(800, 200, 250, 50), color(200), color(50));
Button DisplayItitialSolution = new Button("Show Initial", new Rectangle(800, 260, 250, 50), color(200), color(50));
Button [] SACompletedButtons = {
  DisplayBestSolution, DisplayItitialSolution, ResetSA, NewGame
};

Slider testSlider = new Slider(1200, 600, 300, 10, 0, 10, true);
Slider [] SA_Sliders = {
  testSlider
};

boolean DisplayInitSol_Flag = false;
//Button for return home


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

void mouseClicked() {
  //Mouse operation state machine
  switch(CurrentMenuState) {

  case Menu.HOME:
    if (PausePlaySA.mouseOver(mouseX, mouseY)) {
      AutoPlaySA_Flag = !AutoPlaySA_Flag;
      CurrentMenuState = Menu.SIMULATEDANNEALING;
    }

    if (IterateSA.mouseOver(mouseX, mouseY)) {
      SA_Case.OneIterationOfSimulatedAnnealing();
      graphData.addData(SA_Case.CurrentScore - SA_Case.InitialScore * 0.8);
    }
    
    break;

  case Menu.SIMULATEDANNEALING:
    if (PausePlaySA.mouseOver(mouseX, mouseY)) {
      AutoPlaySA_Flag = !AutoPlaySA_Flag;
    }

    if (IterateSA.mouseOver(mouseX, mouseY)) {
      SA_Case.OneIterationOfSimulatedAnnealing();
      graphData.addData(SA_Case.CurrentScore - SA_Case.InitialScore * 0.8);
    }

    if (ResetSA.mouseOver(mouseX, mouseY)) {
      //Reset the SA to initial
      SA_Case.Reset();
      AutoPlaySA_Flag = false;
      //Reset the graph values
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2);
    }

    if (NewGame.mouseOver(mouseX, mouseY)) {
      AutoPlaySA_Flag = false;
      //generate new game board
      SA_Case = new SimulatedAnnealingState(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings, NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne, Temperature_Min, CoolingRate);

      //reset graph data
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2);
    }
    break;


  case Menu.DONE_SIMULATEDANNEALING:
    if (DisplayBestSolution.mouseOver(mouseX, mouseY)) {
      DisplayInitSol_Flag = false; //Display best solution
    }

    if (DisplayItitialSolution.mouseOver(mouseX, mouseY)) {
      DisplayInitSol_Flag = true; //Display init solution
    }

    if (ResetSA.mouseOver(mouseX, mouseY)) {
      CurrentMenuState = Menu.SIMULATEDANNEALING;
      //Reset the SA to initial
      SA_Case.Reset();
      AutoPlaySA_Flag = false;
      //Reset the graph values
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2);
    }

    if (NewGame.mouseOver(mouseX, mouseY)) {
      CurrentMenuState = Menu.SIMULATEDANNEALING;
      AutoPlaySA_Flag = false;
      //generate new game board
      SA_Case = new SimulatedAnnealingState(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings, NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne, Temperature_Min, CoolingRate);

      //reset graph data
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2);
    }
    break;
  }
}

float testSliderValue;  // Use this variable to capture changing value of slider

void mousePressed() {
  mouseDown();
}

void mouseDragged() {
  mouseDown();
}

void mouseDown() {
  switch(CurrentMenuState) {
  case Menu.HOME:
    break;
  case Menu.SIMULATEDANNEALING:
    testSlider.mouseDown(mouseX, mouseY);
    testSliderValue = testSlider.value;
    break;

  case Menu.DONE_SIMULATEDANNEALING:
    break;
  }
}

void mouseReleased() {
  switch(CurrentMenuState) {
  case Menu.HOME:
    break;
  case Menu.SIMULATEDANNEALING:
    testSlider.mouseUp();
    break;

  case Menu.DONE_SIMULATEDANNEALING:
    break;
  }
}
