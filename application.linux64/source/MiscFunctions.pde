void Draw_SA_Scores() {
  textSize(32);
  fill(255);
  textAlign(LEFT);
  text("BestScore:    ", 700, 30); 
  text("CurrentScore: ", 700, 60);
  text("InitialScore: ", 700, 90);
  textAlign(RIGHT);
  text(SA_Case.BestScore, 1050, 30);
  text(SA_Case.CurrentScore, 1050, 60);
  text(SA_Case.InitialScore, 1050, 90);
}

void Draw_SA_Temperature () {
  textSize(32);
  fill (255);
  textAlign(LEFT);
  text("Current Temp.: ", 1150, 30);
  text("Stopping Temp.: ", 1150, 60);
  text("Cooling Rate: ", 1150, 90);
  textAlign(RIGHT);
  text(SA_Case.Temperature, 1550, 30);
  text(SA_Case.Temperature_Min, 1550, 60);
  text(SA_Case.CoolingRate, 1550, 90);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Setup function

void GameSetup() {
  //Initialize a Simulated Annealing case
  TrigTable = new TrigonmetryLookUpTable(numberOfRingSections);
  SA_Case = new SimulatedAnnealingState(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings, NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne, Temperature_Min, CoolingRate);


  //Initialize a graph values
  graphData = new GraphData();
  graph = new Graph(50, 705, 700, 290, "", "", graphData);

  graphData.addData(SA_Case.InitialScore * 0.2);
}
