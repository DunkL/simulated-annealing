class OneRingSection {
  int Color_ID;
  // int Section_ID; //Is really just and array index, so maybe not needed

  OneRingSection(int NumberOfColors_BetweenOneandNine) { //generate a random ring section
    this.Color_ID = (int) random(0, NumberOfColors_BetweenOneandNine);
  }

  OneRingSection() {
  } //empty class space

  void assignColor(int Color_ID) {
    this.Color_ID = Color_ID;
  }

  void Draw (float x1_DistFromOrigin, float y1_DistFromOrigin, float x2_DistFromOrigin, float y2_DistFromOrigin) {
    //Draw a section (triangle) with a point at origin
    noStroke();
    fill(Color[this.Color_ID]);
    triangle(x_Origin, y_Origin, x_Origin + x1_DistFromOrigin, y_Origin + y1_DistFromOrigin, x_Origin + x2_DistFromOrigin, y_Origin + y2_DistFromOrigin);
  }   

  int Score(int RingID, int SectionID, AllRings AllRingInstance) {
    //Score for one ring section
    int SelectedSectionColor = this.Color_ID;
    int SelectedSectionScore = 0;

    int TotalNumberOfRings = AllRingInstance.ArrayOfAllRings.length;
    int numberOfRingSections = AllRingInstance.ArrayOfAllRings[0].ArrayOfRingSections.length;

    //Boolean checks
    boolean Add_toRingID = (RingID < TotalNumberOfRings-1);
    boolean Sub_toRingID = (RingID > 0);
    boolean Add_toSectionID = (SectionID < numberOfRingSections - 1);
    boolean Sub_toSectionID = (SectionID > 0);
    boolean Zero_SectionID = (SectionID == 0);
    boolean Max_SectionID = (SectionID == numberOfRingSections-1);
    //boolean Exceed_Zero_Section = (SectionID-1 < 0);
    //boolean Exceed_Max_Section = (SectionID+1 > numberOfRingSections);

    //Regular adjacent cases
    if (Add_toRingID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[SectionID].Color_ID)) { 
      SelectedSectionScore+=2;
    }
    if (Sub_toRingID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[SectionID].Color_ID)) { 
      SelectedSectionScore+=2;
    }
    //if (Add_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[SectionID+1].Color_ID)) { SelectedSectionScore+=2; }
    //if (Sub_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[SectionID-1].Color_ID)) { SelectedSectionScore+=2; }

    // Edge case adjacent cases
    //if (Zero_SectionID && (ArrayOfAllRings[RingID][0] == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[numberOfRingSections-1].Color_ID)) { SelectedSectionScore+=2; }
    //if (Max_SectionID && (ArrayOfAllRings[RingID][0] == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[0].Color_ID)) { SelectedSectionScore+=2; }

    //Regular diagonal cases
    if (Add_toRingID && Add_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[SectionID+1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Add_toRingID && Sub_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[SectionID-1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Add_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[SectionID+1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Sub_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[SectionID-1].Color_ID)) { 
      SelectedSectionScore++;
    }

    //Edge case diagonal cases
    if (Add_toRingID && Zero_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[numberOfRingSections-1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Zero_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[numberOfRingSections-1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Add_toRingID && Max_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[0].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Max_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[0].Color_ID)) { 
      SelectedSectionScore++;
    }

    return SelectedSectionScore;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class TrigonmetryLookUpTable {
  //Reduces computation time
  float [] CosineLookUpTable;
  float [] SineLookUpTable;

  TrigonmetryLookUpTable (int numberOfValuesToComputeMinus1) { //numberOfValuesToComputeMinus1 should equal numberOfRingSections
    //Only ever have to compute trig identities once
    this.CosineLookUpTable = new float[numberOfValuesToComputeMinus1+1];
    this.SineLookUpTable = new float[numberOfValuesToComputeMinus1+1];
    for (int i = 0; i < numberOfValuesToComputeMinus1+1; i++) {
      this.CosineLookUpTable[i] = cos(TAU*i/numberOfValuesToComputeMinus1);
      this.SineLookUpTable[i] = sin(TAU*i/numberOfValuesToComputeMinus1);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class OneRingComplete {
  OneRingSection [] ArrayOfRingSections; 
  //int Ring_ID; //Is really just an array index, so maybe not needed.

  OneRingComplete(int NumberOfColors_BetweenOneandNine, int numberOfRingSections) { //Generate a random ring
    this.ArrayOfRingSections = new OneRingSection[numberOfRingSections];
    for (int i = 0; i < numberOfRingSections; i++) {
      this.ArrayOfRingSections[i] = new OneRingSection(NumberOfColors_BetweenOneandNine);
    }
  }

  void Draw(float Radius, TrigonmetryLookUpTable TrigValues) {
    //Draw one ring
    for (int i = 0; i < ArrayOfRingSections.length; i++) {
      this.ArrayOfRingSections[i].Draw(Radius*TrigValues.CosineLookUpTable[i], Radius*TrigValues.SineLookUpTable[i], Radius*TrigValues.CosineLookUpTable[i+1], Radius*TrigValues.SineLookUpTable[i+1]);
    }
  }

  int Score(int Ring_ID, AllRings AllRingInstance) { //Note: Cost is proportional to negative Score.
    //Score one ring
    int numberOfRingSections = this.ArrayOfRingSections.length;
    int TotalScore = 0;
    for (int Section_ID = 0; Section_ID < numberOfRingSections; Section_ID++) {
      TotalScore = TotalScore + this.ArrayOfRingSections[Section_ID].Score(Ring_ID, Section_ID, AllRingInstance);
    }
    return TotalScore;
  }

  void RotateRing() {
    //Rotate this ring.
    int i;
    OneRingSection tempStorage = this.ArrayOfRingSections[0];
    for (i = 0; i < this.ArrayOfRingSections.length - 1; i++) {
      this.ArrayOfRingSections[i] = this.ArrayOfRingSections[i+1];
    }
    this.ArrayOfRingSections[i] = tempStorage;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class AllRings {
  OneRingComplete [] ArrayOfAllRings;

  AllRings (int NumberOfColors_BetweenOneandNine, int numberOfRingSections, int TotalNumberOfRings) {
    //Generate a random set of rings
    this.ArrayOfAllRings = new OneRingComplete[TotalNumberOfRings];
    for (int i = 0; i < TotalNumberOfRings; i++) {
      this.ArrayOfAllRings[i] = new OneRingComplete(NumberOfColors_BetweenOneandNine, numberOfRingSections);
    }
  }

  AllRings (AllRings Source) {
    //Create a new object that is a clone of a Source object
    this.ArrayOfAllRings = new OneRingComplete[Source.ArrayOfAllRings.length];
    for (int i = 0; i < Source.ArrayOfAllRings.length; i++) {
      this.ArrayOfAllRings[i] = new OneRingComplete(0, Source.ArrayOfAllRings[0].ArrayOfRingSections.length);
      arrayCopy(Source.ArrayOfAllRings[i].ArrayOfRingSections, this.ArrayOfAllRings[i].ArrayOfRingSections);
    }
  }

  void Clone (AllRings Source) {
    //Clone a Source object
    for (int i = 0; i < Source.ArrayOfAllRings.length; i++) {
      arrayCopy(Source.ArrayOfAllRings[i].ArrayOfRingSections, this.ArrayOfAllRings[i].ArrayOfRingSections);
    }
  }

  void Draw(int Radius_Max, TrigonmetryLookUpTable TrigValues) {
    //Draw all the rings.
    int Radius_Min = 50;
    int Radius_Diff = Radius_Max - Radius_Min;
    float RingSeparationDistance = (float)Radius_Diff / (float)this.ArrayOfAllRings.length;
    for (int i = 0; i < this.ArrayOfAllRings.length; i++) {
      this.ArrayOfAllRings[i].Draw(Radius_Max - i * RingSeparationDistance, TrigValues);
    }
    //Can be included if 
    //Draw a black central circle
    //fill(color(0, 0, 0));
    //circle(x_Origin, y_Origin, Radius_Min * 2);
  }

  int Score() { //Note: Cost is proportional to negative Score.
    //total score for the entire board
    int TotalNumberOfRings = this.ArrayOfAllRings.length;
    int TotalScore = 0;
    for (int RingID = 0; RingID < TotalNumberOfRings; RingID++) {
      TotalScore = TotalScore + this.ArrayOfAllRings[RingID].Score(RingID, this);
    }
    return TotalScore;
  }

  void SwapTwoRings(int RingID_1, int RingID_2) {
    OneRingComplete tempStorage = this.ArrayOfAllRings[RingID_1];
    this.ArrayOfAllRings[RingID_1] = this.ArrayOfAllRings[RingID_2];
    this.ArrayOfAllRings[RingID_2] = tempStorage;
  }

  void RotateRing(int RingID) {
    this.ArrayOfAllRings[RingID].RotateRing();
  }

  void Perturb() {
    //Randomly select to swap or rotate a ring (generates a neighbour function)
    int rand_Control = (int) random(0, 2);
    if (rand_Control == 0) {
      int rand_RingID_1 = (int) random(0, this.ArrayOfAllRings.length);
      int rand_RingID_2 = (int) random(0, this.ArrayOfAllRings.length);
      this.SwapTwoRings(rand_RingID_1, rand_RingID_2);
    } else {
      int rand_NumberofRotations = (int) random(1, this.ArrayOfAllRings.length);
      int rand_RingID = (int) random(0, this.ArrayOfAllRings.length);
      for (int i = 0; i < rand_NumberofRotations; i++) {
        this.RotateRing(rand_RingID);
      }
    }
  }
}
