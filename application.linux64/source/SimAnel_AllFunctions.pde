class SimulatedAnnealingState {
  AllRings InitialState;
  int InitialScore;
  AllRings CurrentState;
  int CurrentScore;
  AllRings BestState;
  int BestScore;

  float Temperature;
  float Temperature_Min;
  float CoolingRate;
  boolean Temperature_BelowMin;


  SimulatedAnnealingState(int NumberOfColors_BetweenOneandNine, int numberOfRingSections, int TotalNumberOfRings, int NumberOfExperiments, float DesiredAcceptanceRate_BetweenZeroAndOne, float Temperature_Min, float CoolingRate) {
    //Generate a random starting state

    this.InitialState = new AllRings(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings);
    this.CurrentState = new AllRings(InitialState);
    this.BestState = new AllRings(InitialState);

    this.InitialScore = this.InitialState.Score();
    this.CurrentScore = this.InitialScore;
    this.BestScore = this.InitialScore;

    this.SetTemperature_AcceptanceRate(NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne);
    this.Temperature_Min = Temperature_Min;
    this.CoolingRate = CoolingRate;
    this.Temperature_BelowMin = false;
  }

  void Reset () {
    this.CurrentState = new AllRings(InitialState);
    this.BestState = new AllRings(InitialState);

    this.CurrentScore = this.InitialScore;
    this.BestScore = this.InitialScore;

    this.SetTemperature_AcceptanceRate(NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne);
    this.Temperature_BelowMin = false;
  }

  void SetTemperature_AcceptanceRate(int NumberOfExperiments, float DesiredAcceptanceRate_BetweenZeroAndOne) {
    AllRings PreviousState = new AllRings(this.CurrentState);
    int PreviousScore = this.CurrentScore;
    float SumScoreDifference = 0;
    //Do a whole bunch of experiments and average the change in score
    for (int i = 0; i < NumberOfExperiments; i++) {
      this.CurrentState.Perturb();

      int NewScore =  this.CurrentState.Score();
      SumScoreDifference = SumScoreDifference + abs(CurrentScore - PreviousScore);
      PreviousScore = NewScore;
    }

    //Use the average change in score and solve for a temperature that gives you that acceptance rate
    float AverageScoreChange = SumScoreDifference / NumberOfExperiments;
    float SetTemperature = (-1 * AverageScoreChange) / log(DesiredAcceptanceRate_BetweenZeroAndOne);

    this.Temperature = SetTemperature;

    //Return starting state
    this.CurrentState.Clone(PreviousState);
  }


  //Simulated Annealing
  void OneIterationOfSimulatedAnnealing () {
    if (this.Temperature > this.Temperature_Min) {
      //Save the original array
      AllRings PreviousState = new AllRings(this.CurrentState);

      //Perturb the array
      this.CurrentState.Perturb();

      //Delta Cost
      int Score_Previous = this.CurrentScore;
      int Score_Current = this.CurrentState.Score();
      int Score_Difference = Score_Current - Score_Previous;

      //Start Accept/Reject
      if (Score_Difference > 0) { 
        this.CurrentScore = Score_Current; //accept higher scores

        //check if we need to update best score
        if (Score_Current > this.BestScore) {
          this.BestScore = Score_Current;  //update best score
          //println("New Best Score:");
          //println(this.BestScore);
          this.BestState.Clone(this.CurrentState);
        }
      } else {
        float rand_BetweenZeroAndOne = random(0, 1);
        if (rand_BetweenZeroAndOne < exp( (float)Score_Difference / this.Temperature )) {
          this.CurrentScore = Score_Current; //Keep perturbed array and update current score
        } else { //Restore original array
          this.CurrentState.Clone(PreviousState);
        }
      } //End Accept/Reject

      //Cool System
      this.Temperature = this.Temperature * this.CoolingRate;
    } else {
      //If done, set this flag to true
      this.Temperature_BelowMin = true;
    }
  }
}
