class Graph {
  color backgroundColor = color(255, 255, 255);

  GraphData graphData;

  int x;
  int y;
  int w;
  int h;

  String xLabel;
  String yLabel;

  Graph(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  Graph(int x, int y, int w, int h, String xLabel, String yLabel) {
    this(x, y, w, h);
    this.xLabel = xLabel;
    this.yLabel = yLabel;
  }

  Graph(int x, int y, int w, int h, String xLabel, String yLabel, GraphData graphData) {
    this(x, y, w, h, xLabel, yLabel);
    this.graphData = graphData;
  }

  void drawGraph() {
    drawBackground();
    drawAxis();

    if (xLabel != null) {
      drawXAxisLabel();
    }

    if (yLabel != null) {
      drawYAxisLabel();
    }

    if (graphData != null) {
      drawPlot();
    }
  }

  private void drawBackground() {
    noStroke();
    fill(backgroundColor);
    rect(x, y, w, h);
  }


  private void drawAxis() {
    stroke(3);
    line(x, y, x, y + h);
    line(x, y + h, x + w, y + h);

    line(x, y, x + w, y);
    line(x + w, y, x + w, y + h);
  }

  private void drawXAxisLabel() {
  }
  private void drawYAxisLabel() {
  }

  private void drawPlot() {
    float maxScaleMultiplier = 1.2;
    float maxScale = graphData.maxValue() * maxScaleMultiplier;

    stroke(1);

    ArrayList<Float> data = graphData.data;

    if (data.size() == 0) return;

    if (data.size() == 1) {
      //Not sure what this one does Dunk
      float percentage = data.get(0) / maxScale;
      //line(x, y - h * percentage, x + w, y - h * percentage);
      return;
    }

    float xSpacing = (float) w / data.size();

    for (int i = 0; i < data.size() - 1; i++) {
      stroke(1);
      float percentage1 = data.get(i) / maxScale;
      float percentage2 = data.get(i + 1) / maxScale;
      line(x + i * xSpacing, (y + h) - h * percentage1, x + (i + 1) * xSpacing, (y + h) - h * percentage2);
    }
  }
}
