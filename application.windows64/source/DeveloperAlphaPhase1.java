import processing.core.*; 
import processing.data.*; 
import processing.event.*; 
import processing.opengl.*; 

import java.util.HashMap; 
import java.util.ArrayList; 
import java.io.File; 
import java.io.BufferedReader; 
import java.io.PrintWriter; 
import java.io.InputStream; 
import java.io.OutputStream; 
import java.io.IOException; 

public class DeveloperAlphaPhase1 extends PApplet {

//Default Game Settings (edittable)
int NumberOfColors_BetweenOneandNine = 5; //value between 0 and 9
int numberOfRingSections = 10; // value greater than or equal to 3
int TotalNumberOfRings = 10; // value greater than or equal to 1

//Simulated Annealing Settings (editable)
int NumberOfExperiments = 10000; // used in setting an initial temperature any positive value
float DesiredAcceptanceRate_BetweenZeroAndOne = 0.9f; // any float between 0 and 1
float Temperature_Min = 0.8f; // any float greater than 0
float CoolingRate = 0.99f; // any float, recommend to use a value between 0 and 1

//Plan: Have these all editable within the program


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Static global variables

//Available ring colors
int [] Color = {color(255, 0, 0), color(0, 255, 0), color(0, 0, 255), color(255, 255, 0), color(0, 255, 255), color(255, 0, 255), color(255, 255, 255), color(0, 0, 0), color(125, 125, 125)};
static final float x_Origin = 400; //Center of rings x
static final float y_Origin = 350; //Center of rings y
static final int Radius_Max = 350; //Radius of rings

TrigonmetryLookUpTable TrigTable; //Used in drawing ring sections
SimulatedAnnealingState SA_Case;
Graph graph;
GraphData graphData;



///////////////////////////////////////////////////////////////////////////////////////////////////////////


public void setup() {  
  
  GameSetup();
}

public void draw() { 
  //Setup the next frame to be drawn
  background(50);
  graph.drawGraph(); //clear background


  //State drawing machine (starts in Menu.SIMULATEDANNEALING temp for debugging)
  switch(CurrentMenuState) {

  case Menu.HOME:
    drawButtons(SA_Before_During);
    SA_Case.CurrentState.Draw(Radius_Max, TrigTable); //draw current state as placeholder/final, may be drawn over later
    Draw_SA_Scores();
    Draw_SA_Temperature();
    break;


  case Menu.SIMULATEDANNEALING:
    drawButtons(SA_Before_During);
    SA_Case.CurrentState.Draw(Radius_Max, TrigTable); //draw current state as placeholder/final, may be drawn over later
    if ( /* NOT */ ! SA_Case.Temperature_BelowMin && AutoPlaySA_Flag) {
      SA_Case.OneIterationOfSimulatedAnnealing();
      graphData.addData(SA_Case.CurrentScore - SA_Case.InitialScore * 0.8f);
    } else if (SA_Case.Temperature_BelowMin) {
      CurrentMenuState = Menu.DONE_SIMULATEDANNEALING;
    }
    Draw_SA_Scores();
    Draw_SA_Temperature();
    
    drawSliders(SA_Sliders);
    break;


  case Menu.DONE_SIMULATEDANNEALING:
    drawButtons(SACompletedButtons);
    if (DisplayInitSol_Flag) {
      SA_Case.InitialState.Draw(Radius_Max, TrigTable);
    } else {
      SA_Case.BestState.Draw(Radius_Max, TrigTable);
    }
    Draw_SA_Scores();
    Draw_SA_Temperature();
    break;
  }
}





////////////////////////////////////////////
class Button {
  String text;
  int textColor;
  int backgroundColor;

  Rectangle dimensions;

  Button(String text, Rectangle dimensions) {
    this.text = text;
    this.dimensions = dimensions;
  }

  Button(String text, Rectangle dimensions, int backgroundColor) {
    this(text, dimensions);
    this.backgroundColor = backgroundColor;
  }

  Button(String text, Rectangle dimensions, int backgroundColor, int textColor) {
    this(text, dimensions, backgroundColor);
    this.textColor = textColor;
  }

  public boolean mouseOver(int xMouse, int yMouse) {
    return dimensions.pointInside(xMouse, yMouse);
  }

  public void drawButton() {
    fill(backgroundColor);
    rect(dimensions.x, dimensions.y, dimensions.w, dimensions.h);

    fill(textColor);
    textAlign(CENTER, CENTER);
    text(text, dimensions.x + dimensions.w * 0.5f, dimensions.y + dimensions.h * 0.5f);
  }
}
class Graph {
  int backgroundColor = color(255, 255, 255);

  GraphData graphData;

  int x;
  int y;
  int w;
  int h;

  String xLabel;
  String yLabel;

  Graph(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  Graph(int x, int y, int w, int h, String xLabel, String yLabel) {
    this(x, y, w, h);
    this.xLabel = xLabel;
    this.yLabel = yLabel;
  }

  Graph(int x, int y, int w, int h, String xLabel, String yLabel, GraphData graphData) {
    this(x, y, w, h, xLabel, yLabel);
    this.graphData = graphData;
  }

  public void drawGraph() {
    drawBackground();
    drawAxis();

    if (xLabel != null) {
      drawXAxisLabel();
    }

    if (yLabel != null) {
      drawYAxisLabel();
    }

    if (graphData != null) {
      drawPlot();
    }
  }

  private void drawBackground() {
    noStroke();
    fill(backgroundColor);
    rect(x, y, w, h);
  }


  private void drawAxis() {
    stroke(3);
    line(x, y, x, y + h);
    line(x, y + h, x + w, y + h);

    line(x, y, x + w, y);
    line(x + w, y, x + w, y + h);
  }

  private void drawXAxisLabel() {
  }
  private void drawYAxisLabel() {
  }

  private void drawPlot() {
    float maxScaleMultiplier = 1.2f;
    float maxScale = graphData.maxValue() * maxScaleMultiplier;

    stroke(1);

    ArrayList<Float> data = graphData.data;

    if (data.size() == 0) return;

    if (data.size() == 1) {
      //Not sure what this one does Dunk
      float percentage = data.get(0) / maxScale;
      //line(x, y - h * percentage, x + w, y - h * percentage);
      return;
    }

    float xSpacing = (float) w / data.size();

    for (int i = 0; i < data.size() - 1; i++) {
      stroke(1);
      float percentage1 = data.get(i) / maxScale;
      float percentage2 = data.get(i + 1) / maxScale;
      line(x + i * xSpacing, (y + h) - h * percentage1, x + (i + 1) * xSpacing, (y + h) - h * percentage2);
    }
  }
}
class GraphData {
  ArrayList<Float> data = new ArrayList<Float>();

  GraphData () {
  }

  public void addData(float value) {
    data.add(value);
  }

  public float maxValue() {
    float max = Integer.MIN_VALUE;
    for (int i = 0; i<data.size(); i++) {
      if (data.get(i) > max) {
        max = data.get(i);
      }
    }
    return max;
  }
}
//Menu states
int CurrentMenuState = Menu.SIMULATEDANNEALING;//tempfor debugging //HOME; //Initialize menu state to home

static abstract class Menu {
  static final int HOME = 0;
  static final int SIMULATEDANNEALING = 1;
  static final int DONE_SIMULATEDANNEALING = 2;
}

//Button draw menu screen from button array
public void drawButtons(Button [] currentButtons) {
  for (int i = 0; i < currentButtons.length; i++) {
    currentButtons[i].drawButton();
  }
}

public void drawSliders(Slider [] currentSliders) {
  for (int i = 0; i < currentSliders.length; i++) {
    currentSliders[i].drawSlider();
  }
}

/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Button arrays

//Global Buttons
Button ResetSA = new Button("Reset", new Rectangle(800, 320, 250, 50), color(200), color(50));
Button NewGame = new Button("Create New Game", new Rectangle(800, 380, 250, 50), color(200), color(50));
//Button for return home


//Home menu buttons
//CurrentMenuState = Menu.HOME
//Button for how to play
//Button for play
//Button for challenge board
//Button for credits??



//How to play
//Next screen
//return Home
//game start
//rotation
//swap
//scoring


//Play settings menu
//Number of rings
//Number of sections
//Game mode selection (solo, computer simulation)


//Button while SA is active
//CurrentMenuState = Menu.SIMULATEDANNEALING
Button PausePlaySA = new Button("Pause/Play", new Rectangle(800, 200, 250, 50), color(200), color(50));
Button IterateSA = new Button("Do 1 Iteration", new Rectangle(800, 260, 250, 50), color(200), color(50));
Button [] SA_Before_During = {
  PausePlaySA, IterateSA, ResetSA, NewGame
};

boolean AutoPlaySA_Flag = false;
//Sliders SA settings (set init temp/desired acceptance, min temp, cooling rate (0<1)
//Apply new settings
//Button for return home


//Buttons when SA is done
//CurrentMenuState = Menu.DONE_SIMULATEDANNEALING
Button DisplayBestSolution = new Button("Show Best", new Rectangle(800, 200, 250, 50), color(200), color(50));
Button DisplayItitialSolution = new Button("Show Initial", new Rectangle(800, 260, 250, 50), color(200), color(50));
Button [] SACompletedButtons = {
  DisplayBestSolution, DisplayItitialSolution, ResetSA, NewGame
};

Slider testSlider = new Slider(1200, 600, 300, 10, 0, 10, true);
Slider [] SA_Sliders = {
  testSlider
};

boolean DisplayInitSol_Flag = false;
//Button for return home


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

public void mouseClicked() {
  //Mouse operation state machine
  switch(CurrentMenuState) {

  case Menu.HOME:
    if (PausePlaySA.mouseOver(mouseX, mouseY)) {
      AutoPlaySA_Flag = !AutoPlaySA_Flag;
      CurrentMenuState = Menu.SIMULATEDANNEALING;
    }

    if (IterateSA.mouseOver(mouseX, mouseY)) {
      SA_Case.OneIterationOfSimulatedAnnealing();
      graphData.addData(SA_Case.CurrentScore - SA_Case.InitialScore * 0.8f);
    }
    
    break;

  case Menu.SIMULATEDANNEALING:
    if (PausePlaySA.mouseOver(mouseX, mouseY)) {
      AutoPlaySA_Flag = !AutoPlaySA_Flag;
    }

    if (IterateSA.mouseOver(mouseX, mouseY)) {
      SA_Case.OneIterationOfSimulatedAnnealing();
      graphData.addData(SA_Case.CurrentScore - SA_Case.InitialScore * 0.8f);
    }

    if (ResetSA.mouseOver(mouseX, mouseY)) {
      //Reset the SA to initial
      SA_Case.Reset();
      AutoPlaySA_Flag = false;
      //Reset the graph values
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2f);
    }

    if (NewGame.mouseOver(mouseX, mouseY)) {
      AutoPlaySA_Flag = false;
      //generate new game board
      SA_Case = new SimulatedAnnealingState(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings, NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne, Temperature_Min, CoolingRate);

      //reset graph data
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2f);
    }
    break;


  case Menu.DONE_SIMULATEDANNEALING:
    if (DisplayBestSolution.mouseOver(mouseX, mouseY)) {
      DisplayInitSol_Flag = false; //Display best solution
    }

    if (DisplayItitialSolution.mouseOver(mouseX, mouseY)) {
      DisplayInitSol_Flag = true; //Display init solution
    }

    if (ResetSA.mouseOver(mouseX, mouseY)) {
      CurrentMenuState = Menu.SIMULATEDANNEALING;
      //Reset the SA to initial
      SA_Case.Reset();
      AutoPlaySA_Flag = false;
      //Reset the graph values
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2f);
    }

    if (NewGame.mouseOver(mouseX, mouseY)) {
      CurrentMenuState = Menu.SIMULATEDANNEALING;
      AutoPlaySA_Flag = false;
      //generate new game board
      SA_Case = new SimulatedAnnealingState(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings, NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne, Temperature_Min, CoolingRate);

      //reset graph data
      graphData = new GraphData();
      graph = new Graph(50, 705, 700, 290, "", "", graphData);
      graphData.addData(SA_Case.InitialScore * 0.2f);
    }
    break;
  }
}

float testSliderValue;  // Use this variable to capture changing value of slider

public void mousePressed() {
  mouseDown();
}

public void mouseDragged() {
  mouseDown();
}

public void mouseDown() {
  switch(CurrentMenuState) {
  case Menu.HOME:
    break;
  case Menu.SIMULATEDANNEALING:
    testSlider.mouseDown(mouseX, mouseY);
    testSliderValue = testSlider.value;
    break;

  case Menu.DONE_SIMULATEDANNEALING:
    break;
  }
}

public void mouseReleased() {
  switch(CurrentMenuState) {
  case Menu.HOME:
    break;
  case Menu.SIMULATEDANNEALING:
    testSlider.mouseUp();
    break;

  case Menu.DONE_SIMULATEDANNEALING:
    break;
  }
}
public void Draw_SA_Scores() {
  textSize(32);
  fill(255);
  textAlign(LEFT);
  text("BestScore:    ", 700, 30); 
  text("CurrentScore: ", 700, 60);
  text("InitialScore: ", 700, 90);
  textAlign(RIGHT);
  text(SA_Case.BestScore, 1050, 30);
  text(SA_Case.CurrentScore, 1050, 60);
  text(SA_Case.InitialScore, 1050, 90);
}

public void Draw_SA_Temperature () {
  textSize(32);
  fill (255);
  textAlign(LEFT);
  text("Current Temp.: ", 1150, 30);
  text("Stopping Temp.: ", 1150, 60);
  text("Cooling Rate: ", 1150, 90);
  textAlign(RIGHT);
  text(SA_Case.Temperature, 1550, 30);
  text(SA_Case.Temperature_Min, 1550, 60);
  text(SA_Case.CoolingRate, 1550, 90);
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
//Setup function

public void GameSetup() {
  //Initialize a Simulated Annealing case
  TrigTable = new TrigonmetryLookUpTable(numberOfRingSections);
  SA_Case = new SimulatedAnnealingState(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings, NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne, Temperature_Min, CoolingRate);


  //Initialize a graph values
  graphData = new GraphData();
  graph = new Graph(50, 705, 700, 290, "", "", graphData);

  graphData.addData(SA_Case.InitialScore * 0.2f);
}
class Rectangle {
  int x, y, w, h;

  Rectangle(int x, int y, int w, int h) {
    this.x = x;
    this.y = y;
    this.w = w;
    this.h = h;
  }

  public boolean pointInside(int pointX, int pointY) {
    return pointX > x && pointX < x + w && pointY > y && pointY < y + h;
  }
}
class OneRingSection {
  int Color_ID;
  // int Section_ID; //Is really just and array index, so maybe not needed

  OneRingSection(int NumberOfColors_BetweenOneandNine) { //generate a random ring section
    this.Color_ID = (int) random(0, NumberOfColors_BetweenOneandNine);
  }

  OneRingSection() {
  } //empty class space

  public void assignColor(int Color_ID) {
    this.Color_ID = Color_ID;
  }

  public void Draw (float x1_DistFromOrigin, float y1_DistFromOrigin, float x2_DistFromOrigin, float y2_DistFromOrigin) {
    //Draw a section (triangle) with a point at origin
    noStroke();
    fill(Color[this.Color_ID]);
    triangle(x_Origin, y_Origin, x_Origin + x1_DistFromOrigin, y_Origin + y1_DistFromOrigin, x_Origin + x2_DistFromOrigin, y_Origin + y2_DistFromOrigin);
  }   

  public int Score(int RingID, int SectionID, AllRings AllRingInstance) {
    //Score for one ring section
    int SelectedSectionColor = this.Color_ID;
    int SelectedSectionScore = 0;

    int TotalNumberOfRings = AllRingInstance.ArrayOfAllRings.length;
    int numberOfRingSections = AllRingInstance.ArrayOfAllRings[0].ArrayOfRingSections.length;

    //Boolean checks
    boolean Add_toRingID = (RingID < TotalNumberOfRings-1);
    boolean Sub_toRingID = (RingID > 0);
    boolean Add_toSectionID = (SectionID < numberOfRingSections - 1);
    boolean Sub_toSectionID = (SectionID > 0);
    boolean Zero_SectionID = (SectionID == 0);
    boolean Max_SectionID = (SectionID == numberOfRingSections-1);
    //boolean Exceed_Zero_Section = (SectionID-1 < 0);
    //boolean Exceed_Max_Section = (SectionID+1 > numberOfRingSections);

    //Regular adjacent cases
    if (Add_toRingID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[SectionID].Color_ID)) { 
      SelectedSectionScore+=2;
    }
    if (Sub_toRingID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[SectionID].Color_ID)) { 
      SelectedSectionScore+=2;
    }
    //if (Add_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[SectionID+1].Color_ID)) { SelectedSectionScore+=2; }
    //if (Sub_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[SectionID-1].Color_ID)) { SelectedSectionScore+=2; }

    // Edge case adjacent cases
    //if (Zero_SectionID && (ArrayOfAllRings[RingID][0] == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[numberOfRingSections-1].Color_ID)) { SelectedSectionScore+=2; }
    //if (Max_SectionID && (ArrayOfAllRings[RingID][0] == AllRingInstance.ArrayOfAllRings[RingID].ArrayOfRingSections[0].Color_ID)) { SelectedSectionScore+=2; }

    //Regular diagonal cases
    if (Add_toRingID && Add_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[SectionID+1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Add_toRingID && Sub_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[SectionID-1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Add_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[SectionID+1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Sub_toSectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[SectionID-1].Color_ID)) { 
      SelectedSectionScore++;
    }

    //Edge case diagonal cases
    if (Add_toRingID && Zero_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[numberOfRingSections-1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Zero_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[numberOfRingSections-1].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Add_toRingID && Max_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID+1].ArrayOfRingSections[0].Color_ID)) { 
      SelectedSectionScore++;
    }
    if (Sub_toRingID && Max_SectionID && (SelectedSectionColor == AllRingInstance.ArrayOfAllRings[RingID-1].ArrayOfRingSections[0].Color_ID)) { 
      SelectedSectionScore++;
    }

    return SelectedSectionScore;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class TrigonmetryLookUpTable {
  //Reduces computation time
  float [] CosineLookUpTable;
  float [] SineLookUpTable;

  TrigonmetryLookUpTable (int numberOfValuesToComputeMinus1) { //numberOfValuesToComputeMinus1 should equal numberOfRingSections
    //Only ever have to compute trig identities once
    this.CosineLookUpTable = new float[numberOfValuesToComputeMinus1+1];
    this.SineLookUpTable = new float[numberOfValuesToComputeMinus1+1];
    for (int i = 0; i < numberOfValuesToComputeMinus1+1; i++) {
      this.CosineLookUpTable[i] = cos(TAU*i/numberOfValuesToComputeMinus1);
      this.SineLookUpTable[i] = sin(TAU*i/numberOfValuesToComputeMinus1);
    }
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class OneRingComplete {
  OneRingSection [] ArrayOfRingSections; 
  //int Ring_ID; //Is really just an array index, so maybe not needed.

  OneRingComplete(int NumberOfColors_BetweenOneandNine, int numberOfRingSections) { //Generate a random ring
    this.ArrayOfRingSections = new OneRingSection[numberOfRingSections];
    for (int i = 0; i < numberOfRingSections; i++) {
      this.ArrayOfRingSections[i] = new OneRingSection(NumberOfColors_BetweenOneandNine);
    }
  }

  public void Draw(float Radius, TrigonmetryLookUpTable TrigValues) {
    //Draw one ring
    for (int i = 0; i < ArrayOfRingSections.length; i++) {
      this.ArrayOfRingSections[i].Draw(Radius*TrigValues.CosineLookUpTable[i], Radius*TrigValues.SineLookUpTable[i], Radius*TrigValues.CosineLookUpTable[i+1], Radius*TrigValues.SineLookUpTable[i+1]);
    }
  }

  public int Score(int Ring_ID, AllRings AllRingInstance) { //Note: Cost is proportional to negative Score.
    //Score one ring
    int numberOfRingSections = this.ArrayOfRingSections.length;
    int TotalScore = 0;
    for (int Section_ID = 0; Section_ID < numberOfRingSections; Section_ID++) {
      TotalScore = TotalScore + this.ArrayOfRingSections[Section_ID].Score(Ring_ID, Section_ID, AllRingInstance);
    }
    return TotalScore;
  }

  public void RotateRing() {
    //Rotate this ring.
    int i;
    OneRingSection tempStorage = this.ArrayOfRingSections[0];
    for (i = 0; i < this.ArrayOfRingSections.length - 1; i++) {
      this.ArrayOfRingSections[i] = this.ArrayOfRingSections[i+1];
    }
    this.ArrayOfRingSections[i] = tempStorage;
  }
}

//////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

class AllRings {
  OneRingComplete [] ArrayOfAllRings;

  AllRings (int NumberOfColors_BetweenOneandNine, int numberOfRingSections, int TotalNumberOfRings) {
    //Generate a random set of rings
    this.ArrayOfAllRings = new OneRingComplete[TotalNumberOfRings];
    for (int i = 0; i < TotalNumberOfRings; i++) {
      this.ArrayOfAllRings[i] = new OneRingComplete(NumberOfColors_BetweenOneandNine, numberOfRingSections);
    }
  }

  AllRings (AllRings Source) {
    //Create a new object that is a clone of a Source object
    this.ArrayOfAllRings = new OneRingComplete[Source.ArrayOfAllRings.length];
    for (int i = 0; i < Source.ArrayOfAllRings.length; i++) {
      this.ArrayOfAllRings[i] = new OneRingComplete(0, Source.ArrayOfAllRings[0].ArrayOfRingSections.length);
      arrayCopy(Source.ArrayOfAllRings[i].ArrayOfRingSections, this.ArrayOfAllRings[i].ArrayOfRingSections);
    }
  }

  public void Clone (AllRings Source) {
    //Clone a Source object
    for (int i = 0; i < Source.ArrayOfAllRings.length; i++) {
      arrayCopy(Source.ArrayOfAllRings[i].ArrayOfRingSections, this.ArrayOfAllRings[i].ArrayOfRingSections);
    }
  }

  public void Draw(int Radius_Max, TrigonmetryLookUpTable TrigValues) {
    //Draw all the rings.
    int Radius_Min = 50;
    int Radius_Diff = Radius_Max - Radius_Min;
    float RingSeparationDistance = (float)Radius_Diff / (float)this.ArrayOfAllRings.length;
    for (int i = 0; i < this.ArrayOfAllRings.length; i++) {
      this.ArrayOfAllRings[i].Draw(Radius_Max - i * RingSeparationDistance, TrigValues);
    }
    //Can be included if 
    //Draw a black central circle
    //fill(color(0, 0, 0));
    //circle(x_Origin, y_Origin, Radius_Min * 2);
  }

  public int Score() { //Note: Cost is proportional to negative Score.
    //total score for the entire board
    int TotalNumberOfRings = this.ArrayOfAllRings.length;
    int TotalScore = 0;
    for (int RingID = 0; RingID < TotalNumberOfRings; RingID++) {
      TotalScore = TotalScore + this.ArrayOfAllRings[RingID].Score(RingID, this);
    }
    return TotalScore;
  }

  public void SwapTwoRings(int RingID_1, int RingID_2) {
    OneRingComplete tempStorage = this.ArrayOfAllRings[RingID_1];
    this.ArrayOfAllRings[RingID_1] = this.ArrayOfAllRings[RingID_2];
    this.ArrayOfAllRings[RingID_2] = tempStorage;
  }

  public void RotateRing(int RingID) {
    this.ArrayOfAllRings[RingID].RotateRing();
  }

  public void Perturb() {
    //Randomly select to swap or rotate a ring (generates a neighbour function)
    int rand_Control = (int) random(0, 2);
    if (rand_Control == 0) {
      int rand_RingID_1 = (int) random(0, this.ArrayOfAllRings.length);
      int rand_RingID_2 = (int) random(0, this.ArrayOfAllRings.length);
      this.SwapTwoRings(rand_RingID_1, rand_RingID_2);
    } else {
      int rand_NumberofRotations = (int) random(1, this.ArrayOfAllRings.length);
      int rand_RingID = (int) random(0, this.ArrayOfAllRings.length);
      for (int i = 0; i < rand_NumberofRotations; i++) {
        this.RotateRing(rand_RingID);
      }
    }
  }
}
class SimulatedAnnealingState {
  AllRings InitialState;
  int InitialScore;
  AllRings CurrentState;
  int CurrentScore;
  AllRings BestState;
  int BestScore;

  float Temperature;
  float Temperature_Min;
  float CoolingRate;
  boolean Temperature_BelowMin;


  SimulatedAnnealingState(int NumberOfColors_BetweenOneandNine, int numberOfRingSections, int TotalNumberOfRings, int NumberOfExperiments, float DesiredAcceptanceRate_BetweenZeroAndOne, float Temperature_Min, float CoolingRate) {
    //Generate a random starting state

    this.InitialState = new AllRings(NumberOfColors_BetweenOneandNine, numberOfRingSections, TotalNumberOfRings);
    this.CurrentState = new AllRings(InitialState);
    this.BestState = new AllRings(InitialState);

    this.InitialScore = this.InitialState.Score();
    this.CurrentScore = this.InitialScore;
    this.BestScore = this.InitialScore;

    this.SetTemperature_AcceptanceRate(NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne);
    this.Temperature_Min = Temperature_Min;
    this.CoolingRate = CoolingRate;
    this.Temperature_BelowMin = false;
  }

  public void Reset () {
    this.CurrentState = new AllRings(InitialState);
    this.BestState = new AllRings(InitialState);

    this.CurrentScore = this.InitialScore;
    this.BestScore = this.InitialScore;

    this.SetTemperature_AcceptanceRate(NumberOfExperiments, DesiredAcceptanceRate_BetweenZeroAndOne);
    this.Temperature_BelowMin = false;
  }

  public void SetTemperature_AcceptanceRate(int NumberOfExperiments, float DesiredAcceptanceRate_BetweenZeroAndOne) {
    AllRings PreviousState = new AllRings(this.CurrentState);
    int PreviousScore = this.CurrentScore;
    float SumScoreDifference = 0;
    //Do a whole bunch of experiments and average the change in score
    for (int i = 0; i < NumberOfExperiments; i++) {
      this.CurrentState.Perturb();

      int NewScore =  this.CurrentState.Score();
      SumScoreDifference = SumScoreDifference + abs(CurrentScore - PreviousScore);
      PreviousScore = NewScore;
    }

    //Use the average change in score and solve for a temperature that gives you that acceptance rate
    float AverageScoreChange = SumScoreDifference / NumberOfExperiments;
    float SetTemperature = (-1 * AverageScoreChange) / log(DesiredAcceptanceRate_BetweenZeroAndOne);

    this.Temperature = SetTemperature;

    //Return starting state
    this.CurrentState.Clone(PreviousState);
  }


  //Simulated Annealing
  public void OneIterationOfSimulatedAnnealing () {
    if (this.Temperature > this.Temperature_Min) {
      //Save the original array
      AllRings PreviousState = new AllRings(this.CurrentState);

      //Perturb the array
      this.CurrentState.Perturb();

      //Delta Cost
      int Score_Previous = this.CurrentScore;
      int Score_Current = this.CurrentState.Score();
      int Score_Difference = Score_Current - Score_Previous;

      //Start Accept/Reject
      if (Score_Difference > 0) { 
        this.CurrentScore = Score_Current; //accept higher scores

        //check if we need to update best score
        if (Score_Current > this.BestScore) {
          this.BestScore = Score_Current;  //update best score
          //println("New Best Score:");
          //println(this.BestScore);
          this.BestState.Clone(this.CurrentState);
        }
      } else {
        float rand_BetweenZeroAndOne = random(0, 1);
        if (rand_BetweenZeroAndOne < exp( (float)Score_Difference / this.Temperature )) {
          this.CurrentScore = Score_Current; //Keep perturbed array and update current score
        } else { //Restore original array
          this.CurrentState.Clone(PreviousState);
        }
      } //End Accept/Reject

      //Cool System
      this.Temperature = this.Temperature * this.CoolingRate;
    } else {
      //If done, set this flag to true
      this.Temperature_BelowMin = true;
    }
  }
}
class Slider {
  int centerX;
  int centerY;
  
  private int barLength;
  private int barThickness;
  private boolean horizontalOrientation = true;
  int barColor = color(0, 0, 0);
  
  int indicatorSize = 20;                        // Indicator is the triangle
  int indicatorColor = color(240, 240, 240);
  
  private float min = 0;
  private float max = 1;
  
  float value;
  
  private boolean dragging = false;
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max) throws IllegalArgumentException{
    centerX = x;
    centerY = y;
    this.barLength = barLength;
    this.barThickness = barThickness;
    
    if(barLength < 0 || barThickness < 0) throw new IllegalArgumentException("Dimensions must be positive");
    if(min > max) throw new IllegalArgumentException("Min cannot be greater than max");
    
    this.min = min;
    this.max = max;
  }
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max, float initialValue) throws IllegalArgumentException {
    this(x, y, barLength, barThickness, min, max);
    value = initialValue;
  }
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max, boolean horizontalOrientation) throws IllegalArgumentException {
    this(x, y, barLength, barThickness, min, max);
    this.horizontalOrientation = horizontalOrientation;
  }
  
  Slider(int x, int y, int barLength, int barThickness, float min, float max, float initialValue, boolean horizontalOrientation) throws IllegalArgumentException {
    this(x, y, barLength, barThickness, min, max, initialValue);
    this.horizontalOrientation = horizontalOrientation;
  }
  
  private float range() {
    return max - min;
  }
  
  private float centerValue() {
    return (max + min) * 0.5f;
  }
  
  public void mouseDown(int xMouse, int yMouse) {
    if (sliderArea().pointInside(xMouse, yMouse) || dragging) {
      value = mouseSliderValue(xMouse, yMouse);
      
      if (value > max) value = max;
      else if (value < min) value = min;
      
      dragging = true;
    }
  }
  
  public void mouseUp() {
    dragging = false;
  }
  
  public Rectangle sliderArea() {
    if (horizontalOrientation) {
      return new Rectangle(PApplet.parseInt(centerX - barLength * 0.5f), PApplet.parseInt(centerY - barThickness * 0.5f), barLength, barThickness);
    } else {
      return new Rectangle(PApplet.parseInt(centerX - barThickness * 0.5f), PApplet.parseInt(centerY - barLength * 0.5f), barThickness, barLength);
    }
  }
  
  private float mouseSliderValue(int xMouse, int yMouse) {
    return centerValue() + mouseSliderFraction(xMouse, yMouse) * range() * 0.5f;
  }
  
  private float mouseSliderFraction(int xMouse, int yMouse) {  // between -1 to 1
    if (horizontalOrientation) {
      return PApplet.parseFloat(xMouse - centerX) / barLength * 2;
    } else {
      return PApplet.parseFloat(yMouse - centerY) / barLength * 2;
    }
  }
  
  
  
  public void drawSlider() {
    drawBar();
    drawIndicator();
  }
  
  private void drawBar() {
    fill(barColor);
    Rectangle bar = sliderArea();
    rect(bar.x, bar.y, bar.w, bar.h);
  }
  
  private void drawIndicator() {
    fill(indicatorColor);
    float startingAngle = PI * 0.5f;
    float indicatorOffset = (value - centerValue()) / range() * barLength;
    
    if (horizontalOrientation) {
      triangle(
        centerX + indicatorOffset + indicatorSize * cos(startingAngle),
        centerY + indicatorSize * sin(startingAngle), 
        centerX + indicatorOffset + indicatorSize * cos(startingAngle + PI * 2/3), 
        centerY + indicatorSize * sin(startingAngle + PI * 2/3), 
        centerX + indicatorOffset + indicatorSize * cos(startingAngle + PI * 4/3), 
        centerY + indicatorSize * sin(startingAngle + PI * 4/3)
      );
    } else {
      startingAngle = 0;
      triangle(
        centerX + indicatorSize * cos(startingAngle),
        centerY + indicatorOffset + indicatorSize * sin(startingAngle), 
        centerX + indicatorSize * cos(startingAngle + PI * 2/3), 
        centerY + indicatorOffset + indicatorSize * sin(startingAngle + PI * 2/3), 
        centerX + indicatorSize * cos(startingAngle + PI * 4/3), 
        centerY + indicatorOffset + indicatorSize * sin(startingAngle + PI * 4/3)
      );
    }
  }
}
  public void settings() {  size(1600, 1000); }
  static public void main(String[] passedArgs) {
    String[] appletArgs = new String[] { "--present", "--window-color=#666666", "--stop-color=#cccccc", "DeveloperAlphaPhase1" };
    if (passedArgs != null) {
      PApplet.main(concat(appletArgs, passedArgs));
    } else {
      PApplet.main(appletArgs);
    }
  }
}
